<?php

namespace Drupal\multi_image_formatter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\drupal_dam\Plugin\media\Source\DAMFile;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\media\Entity\MediaType;
use Drupal\media\MediaInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AssetFormatController extends ControllerBase {

  /**
   * @var FileSystemInterface
   */
  protected $fileSystem;

  /**
   * @param FileSystemInterface $file_system
   */
  public function __construct(FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
  }

    /**
     * {@inheritdoc}
     */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system')
    );
  }

    /**
     * Route title callback
     *
     * see @Drupal\Core\Entity\Contoller\EntityViewController::view
     *
     * @param EntityInterface $_entity
     * @param string $view_mode
     * @return array
     *   the asset format page title as render array
     */
  public function assetFormatPageTitle(EntityInterface $_entity, $view_mode = 'full') {
    return [
      '#markup' => $this->t('Asset format overview - @label', ['@label' => $_entity->label()])
    ];
  }

  /**
   * directly download media assets file
   *
   * @param EntityInterface $media
   * @return BinaryFileResponse
   */
  public function assetDownload(EntityInterface $media) {
    if ($media instanceof MediaInterface) {
      $media_type = MediaType::load($media->bundle());
      $media_source = $media_type->getSource();
      if (class_exists(DAMFile::class) && $media_source instanceof DAMFile) {
        $source_field = $media_source->getLocalCopyFieldDefinition($media_type)->getName();
      }
      else {
        $source_field = $media_source->getSourceFieldDefinition($media_type)->getName();
      }
      if ($media->get($source_field) instanceof FileFieldItemList && !$media->get($source_field)->isEmpty()) {
        $file = $media->get($source_field)->entity;
        $filepath = $this->fileSystem->realpath($file->getFileUri());
        $headers = array(
          'Content-Type' => $file->getMimeType(),
          'Content-Disposition' => 'attachment;filename="'.$file->getFilename().'"',
          'Content-Length' => $file->getSize(),
          'Content-Description' => 'Download'
        );
        return new BinaryFileResponse($filepath, 200, $headers);
      }
    }
    throw new NotFoundHttpException();
  }
}
