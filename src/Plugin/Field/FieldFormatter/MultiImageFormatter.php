<?php

namespace Drupal\multi_image_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\image\ImageStyleInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin for multi image formatter.
 *
 * @FieldFormatter(
 *   id = "multi_image",
 *   label = @Translation("Multi image output"),
 *   field_types = {
 *     "image",
 *   }
 * )
 */
class MultiImageFormatter extends ImageFormatterBase {

  /**
   * @var EntityStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * @var FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * @param $plugin_id
   * @param $plugin_definition
   * @param FieldDefinitionInterface $field_definition
   * @param array $settings
   * @param $label
   * @param $view_mode
   * @param array $third_party_settings
   * @param EntityStorageInterface $image_style_storage
   * @param FileUrlGeneratorInterface $file_url_generator
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityStorageInterface $image_style_storage, FileUrlGeneratorInterface $file_url_generator)
  {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->imageStyleStorage = $image_style_storage;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings()
  {
    return [
        'image_styles' => [],
        'output_link' => TRUE,
        'output_image' => FALSE,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state)
  {
    $element = parent::settingsForm($form, $form_state);

    $image_styles = image_style_options(FALSE);

    $description_link = Link::fromTextAndUrl(
      $this->t('Configure Image Styles'),
      Url::fromRoute('entity.image_style.collection')
    );

    $element['image_styles'] = [
      '#title' => $this->t('Image styles'),
      '#type' => 'checkboxes',
      '#default_value' => $this->getSetting('image_styles'),
      '#options' => $image_styles,
      '#description' => $description_link->toRenderable(),
    ];

    $element['output_link'] = [
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('output_link'),
      '#title' => $this->t('Output link to image in image style')
    ];

    $element['output_image'] = [
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('output_image'),
      '#title' => $this->t('Output image in image style')
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary()
  {
    $summary = [];

    $image_styles = image_style_options(FALSE);
    // Unset possible 'No defined styles' option.
    unset($image_styles['']);
    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    $image_styles_setting = $this->getSetting('image_styles');
    foreach ($image_styles_setting as $image_style_setting)
      if (isset($image_styles[$image_style_setting])) {
        $summary[] = $this->t('Image style: @style', ['@style' => $image_styles[$image_style_setting]]);
      }
    if ($this->getSetting('output_link')) {
      $summary[] = $this->t('Output link');
    }
    if ($this->getSetting('output_image')) {
      $summary[] = $this->t('Output Image');
    }

    return array_merge($summary, parent::settingsSummary());
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode)
  {
    $elements = [];
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    if (empty($images = $this->getEntitiesToView($items, $langcode))) {
      // Early opt-out if the field is empty.
      return $elements;
    }

    $image_styles = $this->getSetting('image_styles');
    foreach ($images as $delta => $image) {
      $image_uri = $image->getFileUri();
      $formats = [];
      foreach ($image_styles as $styledelta => $style) {
        $image_style = $this->imageStyleStorage->load($style);
        if ($image_style instanceof ImageStyleInterface) {
          $style_cache_tags = $image_style->getCacheTags();
          $cache_tags = Cache::mergeTags($style_cache_tags, $image->getCacheTags());

          // Add cacheability metadata from the image and image style.
          $cacheability = CacheableMetadata::createFromObject($image);
          if ($image_style) {
            $cacheability->addCacheableDependency(CacheableMetadata::createFromObject($image_style));
          }

          $formats[$styledelta]['link'] = [
            '#type' => 'link',
            '#title' => $image_style->label(),
            '#url' => Url::fromUri($image_style->buildUrl($image_uri)),
            '#access' => (bool) $this->getSetting('output_link'),
            '#attributes' => [
              'target' => '_blank',
              'download' => TRUE,
            ]
          ];

          $formats[$styledelta]['image'] = [
            '#theme' => 'image_formatter',
            '#item' => $image->_referringItem,
            '#image_style' => $style,
            '#url' => $this->fileUrlGenerator->generate($image_style->buildUri($image_uri)),
            '#cache' => [
              'tags' => $cache_tags,
            ],
            '#access' => (bool) $this->getSetting('output_image'),
            '#attributes' => [
              'target' => '_blank',
            ]
          ];
        }
      }
      $elements[$delta] = [
        '#theme' => 'multi_image_formatter_formats',
        '#content' => $formats
      ];
    }
    return $elements;
  }
}
