<?php

namespace Drupal\multi_image_formatter\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The router service.
   *
   * @var \Symfony\Component\Routing\RouterInterface
   */
  protected $routeProvider;

  /**
   * Constructs a new RouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteProviderInterface $router_provider
   *   The router service.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager, RouteProviderInterface $router_provider) {
    $this->entityTypeManager = $entity_manager;
    $this->routeProvider = $router_provider;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection)
  {
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($route = $this->getEntityAssetFormatsRoute($entity_type)) {
        $collection->add("entity.$entity_type_id.asset_format", $route);
      }
      if ($route = $this->getEntityAssetCropRoute($entity_type)) {
        $collection->add("entity.$entity_type_id.asset_edit", $route);
      }
      if ($entity_type_id === 'media') {
        if ($route = $this->getEntityAssetDownloadRoute($entity_type)) {
          $collection->add("entity.$entity_type_id.asset_download", $route);
        }
      }
    }
  }

  /**
   * get the asset format route
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getEntityAssetFormatsRoute(EntityTypeInterface $entity_type) {
    if ($asset_format = $entity_type->getLinkTemplate('asset-format')) {
      $route = (new Route($asset_format))
        ->addDefaults([
          '_title' => 'Asset format',
          '_title_callback' => '\Drupal\multi_image_formatter\Controller\AssetFormatController::assetFormatPageTitle',
          '_entity_view' => $entity_type->id().'.asset_format',
      ])
        ->addRequirements([
          '_permission' => 'access asset formats',
        ]);

      return $route;
    }
    return NULL;
  }

  /**
   * get the asset edit route
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getEntityAssetCropRoute(EntityTypeInterface $entity_type) {
    if ($asset_crop = $entity_type->getLinkTemplate('asset-edit')) {
      $route = (new Route($asset_crop))
        ->addDefaults([
          '_title' => 'Asset edit',
          '_entity_form' => $entity_type->id().'.asset_edit',
        ])
        ->addRequirements([
          '_permission' => 'access asset formats',
        ]);

      return $route;
    }
    return NULL;
  }

  /**
   * asset file download route
   *
   * @param EntityTypeInterface $entity_type
   * @return Route
   */
  protected function getEntityAssetDownloadRoute(EntityTypeInterface $entity_type) {
    return (new Route('/media/download-asset/{media}'))
      ->addDefaults([
        '_controller' => '\Drupal\multi_image_formatter\Controller\AssetFormatController::assetDownload',
        '_title' => 'Download',
      ])
      ->addRequirements([
        '_permission' => 'access asset formats',
      ])
      ->setRequirement('media', '\d+');
  }
}
