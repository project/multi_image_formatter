<?php

/**
 * Implements hook_theme().
 */
function multi_image_formatter_theme($existing, $type, $theme, $path)
{
  return [
    'multi_image_formatter_formats' => [
      'variables' => [
        'content' => FALSE,
      ]
    ]
  ];
}

/**
 * Implements hook_entity_type_alter().
 */
function multi_image_formatter_entity_type_alter(array &$entity_types)
{
  foreach ($entity_types as $entity_type_id => $entity_type) {
    if ($entity_type_id === 'media' && $entity_type instanceof \Drupal\Core\Entity\ContentEntityType) {
      $entity_type->setLinkTemplate('asset-format', "/$entity_type_id/asset-format/{media}");
      $entity_type->setLinkTemplate('asset-edit', "/$entity_type_id/asset-edit/{media}");
      $entity_type->setLinkTemplate('asset-download', "/$entity_type_id/asset-download/{entity}");
      $default_handler_class = $entity_type->getHandlerClasses()['form']['default'];
      $entity_type->setFormClass('asset_edit', $default_handler_class);
    }
  }
}

/**
 * Implements hook_entity_extra_field_info().
 */
function multi_image_formatter_entity_extra_field_info()
{
  $extra = [];
  foreach (\Drupal\media\Entity\MediaType::loadMultiple() as $bundle) {
    $extra['media'][$bundle->id()]['form']['asset_format_link'] = [
      'label' => t('Link to asset formats'),
      'weight' => 100,
      'visible' => FALSE
    ];
    $extra['media'][$bundle->id()]['display']['asset_edit_link'] = [
      'label' => t('Link to edit asset config'),
      'weight' => 100,
      'visible' => FALSE
    ];
  }
  return $extra;
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function multi_image_formatter_form_media_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id)
{
  $media_form = $form_state->getFormObject();
  if ($media_form instanceof \Drupal\media\MediaForm) {
    if ($media_form->getEntity() instanceof \Drupal\media\MediaInterface && !$media_form->getEntity()->isNew()) {
      if ($form_state->get('form_display')->getComponent('asset_format_link')) {
        $form['asset_format_link'] = [
          '#type' => 'container'
        ];
        $form['asset_format_link']['link'] = [
          '#type' => 'link',
          '#title' => t('View formats of this asset'),
          '#url' => $media_form->getEntity()->toUrl('asset-format'),
        ];
      }
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_view().
 */
function multi_image_formatter_media_view(array &$build, \Drupal\Core\Entity\EntityInterface $entity, \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display, $view_mode) {
  if ($entity instanceof \Drupal\media\MediaInterface && !$entity->isNew()) {
    if ($display->getComponent('asset_edit_link')) {
      $build['asset_edit_link'] = [
        '#type' => 'link',
        '#title' => t('Edit formats of this asset'),
        '#url' => $entity->toUrl('asset-edit')->setOption(
          'query',
          [
            'destination' => $entity->toUrl('asset-format')->getInternalPath()
          ]
        ),
      ];
    }
  }
}

/**
 * Implements hook_entity_operation().
 */
function multi_image_formatter_entity_operation(\Drupal\Core\Entity\EntityInterface $entity)
{
  $operations = [];
  if ($entity->access('asset-format') && $entity->hasLinkTemplate('asset-format')) {
    $operations['asset-format'] = [
      'title' => t('Formate'),
      'weight' => 100,
      'url' => $entity->toUrl('asset-format'),
    ];
  }
  if ($entity->access('asset-download') && $entity->hasLinkTemplate('asset-download')) {
    $operations['asset-download'] = [
      'title' => t('Download'),
      'weight' => 100,
      'url' => $entity->toUrl('asset-download'),
    ];
  }
  return $operations;
}

/**
 * Implements hook_ENTITY_TYPE_access().
 */
function multi_image_formatter_media_access(\Drupal\Core\Entity\EntityInterface $entity, $operation, \Drupal\Core\Session\AccountInterface $account) {
  if ($operation === 'asset-format') {
    return $entity->toUrl('asset-format')->access(Drupal::currentUser(), TRUE);
  }
  if ($operation === 'asset-download') {
    return $entity->toUrl('asset-download')->access(Drupal::currentUser(), TRUE);
  }
  return \Drupal\Core\Access\AccessResult::neutral();
}
