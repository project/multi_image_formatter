Module provides a field formatter to output an image field in multiple image styles

Output can be image (img tag) in image style or a download link to the image in given image style

It also adds a link template for media entities, to output media entities in a special view mode. This view mode can
be used together with the field formatter (as any other view mode can)

A (optional) pseudo/extra field is added to media entities form display to provide a link to the media link template
